import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';

import { CoreModuleState } from './reducers';
import { TaskActionTypes, CreateTask, CompleteTask, DeleteTask, CancelTask, MarkTaskAsPending } from './task.action';

@Injectable()
export class TaskEffect {

  @Effect({ dispatch: false })
  todoChange: Observable<any> = this.actions.pipe(
    ofType<CreateTask | CompleteTask | DeleteTask | CancelTask | MarkTaskAsPending>(
      TaskActionTypes.Cancel,
      TaskActionTypes.Complete,
      TaskActionTypes.Create,
      TaskActionTypes.Delete,
      TaskActionTypes.MarkAsPending),
    flatMap(() => this.store.select('core')),
    map((state: CoreModuleState) => localStorage.setItem('state', JSON.stringify(state.task)))
  );

  constructor(
    private actions: Actions,
    private store: Store<CoreModuleState>
  ) { }
}
