import { Task, TaskStatus } from '../models/Task';
import { TaskActions, TaskActionTypes, DeleteTask, CreateTask, DeleteList } from './task.action';
import { uuid } from 'src/utils';
import { List } from '../models/List';

export interface TaskState {
  tasks: Task[];
  lists: List[];
  selectedListID: string;
  tasksInSelectedList: Task[];
}

const INITIAL_STATE: TaskState = JSON.parse(localStorage.getItem('state')) || {
  tasks: [],
  lists: [{
    id: '0',
    name: 'Default'
  }],
  selectedListID: '0',
  tasksInSelectedList: []
};

export function tasksReducer(state = INITIAL_STATE, action: TaskActions): TaskState {
  console.log(action.type);
  switch (action.type) {
    case TaskActionTypes.Create:
      return createTask(state, action);

    case TaskActionTypes.Delete:
      return deleteTask(state, action);

    case TaskActionTypes.Cancel:
      return markTask(state, action.payload, TaskStatus.Cancelled);

    case TaskActionTypes.Complete:
      return markTask(state, action.payload, TaskStatus.Completed);

    case TaskActionTypes.MarkAsPending:
      return markTask(state, action.payload, TaskStatus.Pending);

    case TaskActionTypes.Load:
      return { ...state, tasks: action.payload };

    case TaskActionTypes.CreateList:
      return createList(state, action);

    case TaskActionTypes.SelectList:
      return { ...state, selectedListID: action.payload, tasksInSelectedList: getTasksFrom(action.payload, state.tasks) };

    case TaskActionTypes.DeleteList:
      return deleteList(state, action);

    default:
      return state;
  }
}

function sortTask(tasks: Task[]): Task[] {
  const pending: Task[] = [];
  const others: Task[] = [];
  tasks.forEach(task => {
    if (task.status === TaskStatus.Pending) {
      pending.push(task);
    } else {
      others.push(task);
    }
  });
  return [
    ...pending.sort((a, b) => sortTaskByDate(a, b)),
    ...others.sort((a, b) => sortTaskByDate(a, b)),
  ];
}

function sortTaskByDate(a: Task, b: Task): number {
  return new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime();
}

function deleteList(state: TaskState, action: DeleteList): TaskState {
  if (action.payload === '0') {
    return state;
  }
  const tasks = sortTask(state.tasks.filter(task => task.list !== action.payload));
  const selectedListID = state.selectedListID !== action.payload ? state.selectedListID : '0';
  return {
    ...state,
    tasks,
    lists: state.lists.filter(list => list.id !== action.payload),
    selectedListID,
    tasksInSelectedList: getTasksFrom(selectedListID, tasks)
  };
}

function createList(state: TaskState, action) {
  const id = uuid();
  return {
    ...state,
    lists: [...state.lists, { name: action.payload, id }],
    selectedListID: id,
    tasksInSelectedList: getTasksFrom(id, state.tasks)
  };
}

function getTasksFrom(listID: string, tasks: Task[]): Task[] {
  return tasks.filter(task => task.list === listID);
}

function markTask(state: TaskState, taskID: string, status: TaskStatus): TaskState {
  const tasks = sortTask(state.tasks.map<Task>((task) => task.id === taskID
    ? { ...task, updatedAt: new Date(), status }
    : task));
  const newState: TaskState = {
    ...state,
    tasks,
    tasksInSelectedList: getTasksFrom(state.selectedListID, tasks)
  };
  return newState;
}

function deleteTask(state: TaskState, action: DeleteTask): TaskState {
  const tasks = sortTask(state.tasks.filter((task) => task.id !== action.payload));
  return {
    ...state,
    tasks,
    tasksInSelectedList: getTasksFrom(state.selectedListID, tasks)
  };
}

function createTask(state: TaskState, action: CreateTask): TaskState {
  const newTask: Task = {
    content: action.payload.taskContent,
    createdAt: new Date(),
    updatedAt: new Date(),
    id: uuid(),
    list: action.payload.parentListID,
    status: TaskStatus.Pending
  };
  const tasks = sortTask([
    ...state.tasks,
    newTask
  ]);
  const newState: TaskState = {
    ...state,
    tasks,
    tasksInSelectedList: getTasksFrom(state.selectedListID, tasks)
  };
  return newState;
}

