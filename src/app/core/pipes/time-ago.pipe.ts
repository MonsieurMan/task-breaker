import { Pipe, PipeTransform } from '@angular/core';
import { distanceInWordsStrict } from 'date-fns';
import { Observable } from 'rxjs';

@Pipe({
  name: 'timeAgo',
  pure: false
})
export class TimeAgoPipe implements PipeTransform {
  private timeAgoValue = '';

  transform(value: Date | string): string {
    return distanceInWordsStrict(value, new Date()) + ' ago';
  }

}
