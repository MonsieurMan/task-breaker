import { Component } from '@angular/core';
import { CoreModuleState, getListState } from './core/state/reducers';
import { Store } from '@ngrx/store';
import { List } from './core/models/List';
import { SelectList, CreateList, DeleteList } from 'src/app/core/state/task.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  lists: List[];
  selectedList: List;

  constructor(private store: Store<CoreModuleState>) {
    this.store.select(getListState).subscribe(listState => {
      this.lists = listState.lists;
      this.selectedList = listState.lists.find(list => listState.selectedListID === list.id);
    });
  }

  selectList(id: string) {
    this.store.dispatch(new SelectList(id));
  }

  createList(name: string) {
    this.store.dispatch(new CreateList(name));
  }

  deleteList(id: string) {
    this.store.dispatch(new DeleteList(id));
  }
}
